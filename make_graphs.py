import numpy as np
import matplotlib.pyplot as plt

test_accuracies = np.load("test-accuracies.npy")
train_accuracies = np.load("train-accuracies.npy")

fig, axis = plt.subplots(2)

# Test
axis[0].set_title("Logistic Regression (Test)")
axis[0].plot(test_accuracies[0])
axis[0].plot(test_accuracies[1])
axis[0].legend(["Active sampling", "Random sampling"])

axis[1].set_title("Decision Tree Classifier (Test)")
axis[1].plot(test_accuracies[2])
axis[1].plot(test_accuracies[3])
axis[1].legend(["Active sampling", "Random sampling"])

# # Train
# axis[1, 0].set_title("Logistic Regression (Train)")
# axis[1, 0].plot(train_accuracies[0])
# axis[1, 0].plot(train_accuracies[1])
# axis[1, 0].legend(["Active sampling", "Random sampling"])

# axis[1, 1].set_title("Decision Tree Classifier (Train)")
# axis[1, 1].plot(train_accuracies[2])
# axis[1, 1].plot(train_accuracies[3])
# axis[1, 1].legend(["Active sampling", "Random sampling"])
  
plt.show()
