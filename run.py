#!/usr/bin/env python3
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
import pandas as pd
import numpy as np
from Oracle import Oracle
from DataLoader import DataLoader
from DataSampler import DataSampler
from QBCModel import QBCModel

RAND_STATE = 42

def get_model_accuracy(model, test_data, test_labels):
    predictions = model.predict(test_data)
    return accuracy_score(test_labels, predictions)  # Calculate model accuracy


if __name__ == "__main__":
    dataloader = DataLoader(path="heart_disease.csv")
    data_array = dataloader.data # Load data to an array
    data = data_array.values[:, :-1]
    data_labels = data_array.values[:, -1]
    # Split data set with sklearn.model_selection.train_test_split, same for init training points
    train_data, test_data, train_labels, test_labels = train_test_split(data, data_labels, test_size=0.20, random_state=RAND_STATE)
    num_init_train_data = 2  # Number of initial training points
    init_train_data, train_data, init_train_labels, train_labels = train_test_split(train_data, train_labels, 
                                    test_size=len(train_data)-num_init_train_data, 
                                    random_state=RAND_STATE, stratify=train_labels)

    oracle = Oracle(labels = train_labels)  # Oracle class


    # Create different models. Maybe using sklearn
    models = [
              lambda: LogisticRegression(penalty='l2', C=1., solver='liblinear', max_iter=300),
              lambda: DecisionTreeClassifier(max_depth=10, random_state=RAND_STATE)
             ]  # [Model, Model2, ...]
    model_test_accuracies = []
    model_train_accuracies = []
    for makeModel in models:
        committee = QBCModel(makeModel, committeeSize=10)
        active_sampler = DataSampler(pool = train_data, ranking_fn = committee.confidence)
        random_sampler = DataSampler(pool = train_data, ranking_fn = lambda data: np.random.RandomState(seed=RAND_STATE).permutation(np.arange(len(data))))
        samplers = [active_sampler, random_sampler]

        for sampler in samplers:
            model = makeModel()
            test_accuracy = []
            train_accuracy = []
            X = init_train_data
            y = init_train_labels
            for i in range(len(train_data)):
            #for i in range(100):
                committee.fit(X, y)
                data, indices = sampler.sample(n=1)
                labels = oracle.query(indices)
                X = np.append(X, data, axis=0)
                y = np.append(y, labels, axis=0)
                model.fit(X, y)
                test_accuracy.append(get_model_accuracy(model, test_data=test_data, test_labels=test_labels))
                train_accuracy.append(get_model_accuracy(model, test_data=train_data, test_labels=train_labels))
            model_test_accuracies.append(test_accuracy)
            model_train_accuracies.append(train_accuracy)

    np.save("test-accuracies.npy", model_test_accuracies)
    np.save("train-accuracies.npy", model_train_accuracies)

