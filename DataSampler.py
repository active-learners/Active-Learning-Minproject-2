class DataSampler:
    '''
    Class used to pick samples for labelling
    '''
    def __init__(self, pool, ranking_fn):
        '''
        :param pool:        The unlabelled data to sample from
        :param ranking_fn:  The function returning an array of 
                            ranked indicies where the last n
                            indecies will be chosen.
        '''
        self.pool = pool
        self.ranking_fn = ranking_fn

    def sample(self, n:int):
        '''
        Method for sampling unlabelled data for labelling
        :param n:   Number of samples to take from the pool
        :returns:   The n last samples from the pool
                    Ranked according to the ranking_fn.
        '''
        # Sample n datapoints (without replacement) according to ranking_fn
        rankedIndecies = self.ranking_fn(self.pool)
        data = self.pool[rankedIndecies[:n]]
        self.pool = self.pool[rankedIndecies[n:]]
        return data, rankedIndecies[:n]
