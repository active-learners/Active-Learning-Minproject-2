import pandas as pd
import numpy as np
import os

class DataLoader:
    def __init__(self, path=None):
        # path locates csv file
        if path==None:
            working_dir=os.getcwd()
            file_name="heart_disease.csv"
            path=working_dir+"\\"+file_name

        data=pd.read_csv(path,delimiter=',')

        #add only the atributes we wish to use, full list of atributes:
        #['row.names', 'sbp', 'tobacco', 'ldl', 'adiposity', 'famhist', 'typea','obesity', 'alcohol', 'age', 'chd']
        self.data=data[['sbp', 'tobacco', 'ldl', 'adiposity', 'typea','obesity', 'alcohol', 'age', 'chd']]
    def __len__(self):
        # Return length of dataset
        return len(self.data)

    def __get__(self, index,type="np"):
        """
        Return patient at index:
        type np return np array: type pd retuns as pd dataframe
        """
        if type=="pd":
            return self.data.iloc[index]
        elif type=="np":
            return self.data.iloc[index].to_numpy()

    def __iter__(self, index):
        # Yield every datapoint
        pass

    def retun_np(self):
        data=self.data.iloc[:,:-1].to_numpy(),self.data.iloc[:].to_numpy()
        lable=self.data.iloc[:,-1].to_numpy(),self.data.iloc[:].to_numpy()
        return data,lable

