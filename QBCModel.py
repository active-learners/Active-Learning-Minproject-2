import numpy as np
import sklearn

# Query by Committe model
class QBCModel:
    def __init__(self, makeModel, committeeSize:int): 
        '''
        :param InternalModel:   The model to use for creating
                                the committee. Use an sklearn model
                                like sklearn.linear_model.LogisticRegression
        :param committeeSize:   Defines the size of the committee i.e.
                                how many versions of the InternalModel
                                that should be trained.
        '''
        self.makeModel = makeModel
        self.committeeSize = committeeSize
        self.models = [] # Initialize models using internalModel.
        self.predictedLabels = []


    def fit(self, Xtrain, ytrain):
        '''
        :param Xtrain:    The features to train the model on
        :param ytrain:    The labels to train the model on
        '''
        #train committee models on bootstrap samples
        models = []
        for i in range(self.committeeSize):
            bootstrapX, bootstrapY = sklearn.utils.resample(Xtrain, ytrain, stratify=ytrain)
            model = self.makeModel()
            model.fit(bootstrapX, bootstrapY)
            models.append(model)
        
        self.models = models

    def predict(self, data):
        '''
        Method for predicting labels for unlabelled data from pool.
        The predictions are saved internally and used for deciding
        which data point in the poolData should be queried in the
        confidence method.

        :param poolData:  Unlabelled data to make predictions on
        '''
        return np.array([m.predict(data) for m in self.models]).mean(axis=0) >= 0.5


    def confidence(self, data):
        '''
        Decides which data point should be chosen for labelling.
        The data point with the the greatest disagreement in the
        committee of models will be chosen.
        The methods fit and predict need to be run before the
        confidence method to generate predictedLabels.
        '''

        predictions = np.array([m.predict(data) for m in self.models]).mean(axis=0)
        confidence = np.abs(predictions - 0.5)
        return np.argsort(confidence)
