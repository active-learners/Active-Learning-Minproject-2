class Oracle:
    def __init__(self, labels):
        self.labels = labels

    def query(self, indices: [int]):
        # Get labels for data points with given indices
        oracle_labels = []
        for indice in indices:
            oracle_labels.append(self.labels[indice])
        return oracle_labels
